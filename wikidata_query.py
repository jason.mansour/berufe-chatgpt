""" Dieses Modul enthält Methoden, um Berufsdaten aus WikiData und DBPedia abzurufen. """

import json
import sys
import urllib.request
from typing import List, Optional, Set

from SPARQLWrapper import JSON, SPARQLWrapper
from .utils import dump_args


DBPEDIA_URL = "http://de.dbpedia.org/sparql"
WIKIDATA_URL = "https://query.wikidata.org/sparql"

WIKIDATA_JOBS_QUERY = """SELECT ?item ?itemLabel ?desc ?url
WHERE
{
   {
    ?item wdt:P31 wd:Q28640 .
  }union{
    ?item wdt:P31 wd:Q778000 .
  }union{
    ?item wdt:P31 wd:Q12737077 .
  }union{
    ?item wdt:P31 wd:Q66811410 .
  }

  optional {
    ?item wdt:P18 ?img .   
    BIND ( str(?img) as ?url ) 
  }
  optional {
    ?item schema:description ?desc .
    FILTER (lang(?desc) = "de")
  }
  
  SERVICE wikibase:label { 
    bd:serviceParam wikibase:language "de". 
  }
  
} LIMIT 10"""


def sparql_query(endpoint_url, query):
    "Helper function to do a SPARQL query."

    user_agent = "WDQS-example Python/%s.%s" % (
        sys.version_info[0], sys.version_info[1])
    # TODO adjust user agent; see https://w.wiki/CX6
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()

@dump_args
def query_wikidata_jobs():
    """ Ruft Daten zu Berufen aus WikiData ab. Da sehr viele Daten abgerufen werden,
        sollte das Ergebnis gecached werden.
    
        Ausgabeformat ist ein dict mit Entity-IRI -> JSON. z.B.:
    
            {
                'http://www.wikidata.org/entity/Q113109': {
                    'description': 'Berufszweig der Schneiderei, der sich mit der Planung '
                                   'und Herstellung von Bekleidungsartikeln befasst',
                    'label': 'Modeschneider',
                    'image': None
                },
                'http://www.wikidata.org/entity/Q113630': {
                    'description': 'ein ehemaliger Ausbildungsberuf',
                    'label': 'Zapfensteiger',
                    'image': None
                },
                ...
            }
    """

    results = sparql_query(WIKIDATA_URL, WIKIDATA_JOBS_QUERY)

    # here we store the collected information
    wikidata = {}

    for result in results["results"]["bindings"]:
        # print (result)
        item = {}
        if "desc" in result.keys():
            item["description"] = result["desc"]["value"]
        else:
            item["description"] = None

        if "itemLabel" in result.keys():
            item["label"] = result["itemLabel"]["value"]
        else:
            item["label"] = None

        if "url" in result.keys():
            item["image"] = result["url"]["value"]
        else:
            item["image"] = None

        wikidata[result["item"]["value"]] = item

    print(len(wikidata), "Berufe in Wikidata gefunden")
    return wikidata


# Hilfmethoden, um Bilder, Beschreibungen und Bezeichnungen aus der
# deutschen und englischen DBpedia zu laden

def load_dbpedia(iri: str, de=False, label=True) -> Optional[str]:
    """ Holt für eine Ressource die Bezeichung oder Beschreibung aus der DBpedia.

        de=True --> nutze deutsche DBpedia
        label=False -->  sammle Beschreibung ein statt Label

        >>> load_dbpedia(iri='http://de.dbpedia.org/resource/Heilpraktiker', de=True)
        'Heilpraktiker'
        >>> load_dbpedia(iri='http://de.dbpedia.org/resource/Heilpraktiker', de=True, label=False)  # doctest: +ELLIPSIS
        'Als Heilpraktiker wird in Deutschland bezeichnet, wer die Heilkunde...'
    """

    if label:
        schema = 'http://www.w3.org/2000/01/rdf-schema#label'
    else:
        schema = 'http://www.w3.org/2000/01/rdf-schema#comment'

    query = f"""select distinct ?l where {{
        <{iri}> <{schema}> ?l .
        filter (lang(?l) = "de")
    }}"""

    if de:
        endpoint_url = "http://de.dbpedia.org/sparql"
    else:
        endpoint_url = "https://dbpedia.org/sparql"

    data = sparql_query(endpoint_url, query)

    if len(data["results"]["bindings"]) > 0:
        return data["results"]["bindings"][0]['l']['value']
    
    return None


def load_dbpedia_depictions(iri: str, de=False) -> Set[str]:
    """
        de=True --> nutze deutsche DBpedia
    """
    query = f"""select distinct ?l where {{
        <{iri}> <http://xmlns.com/foaf/0.1/depiction> ?l .
    }}"""

    if de:
        endpoint_url = "http://de.dbpedia.org/sparql"
    else:
        endpoint_url = "https://dbpedia.org/sparql"

    data = sparql_query(endpoint_url, query)

    imgs = set()
    for binding in data["results"]["bindings"]:
        imgs.add(binding['l']['value'])
    return imgs

# Hilfmethode zum Analysieren der Texte --> Mapping zur deutschen DBpedia

def analyze_text(text: str) -> List[str]:
    "Findet bekannte Entitäten aus der DBpedia in dem Text."
    with urllib.request.urlopen(
        "https://wlo.yovisto.com/services/extract/" + urllib.parse.quote_plus(text)) as url:
        data = json.load(url)
    result = []
    for entity in data['entities']:
        result.append(entity["entity"])
    return result

# Eigentliche Arbeitsmethode zum Einsammeln und Aggregieren der Daten

def describe(text: str, wikidata: dict):
    "Gibt eine Beschreibung eines Berufs (Wikidata Entität) aus."
    # Liste der Entitäten der DE DBpedia
    entities = analyze_text(text.replace("/", " "))

    # finde alle Resourcen die auch in EN DBpedia und Wikidata vorkommen (sameAs Links verfolgen)
    for entity in entities:
        # zum aufsammeln pro Entität
        descriptions = set()
        labels = set()
        images = set()
        links = set()

        resource = "http://de.dbpedia.org/resource/" + entity
        sparql = f"""
            select distinct * where {{
                <{resource}> <http://www.w3.org/2002/07/owl#sameAs> ?o .
            }} LIMIT 1000"""
        data = sparql_query(DBPEDIA_URL, sparql)

        item = "http://de.dbpedia.org/resource/" + entity
        links.add(item)
        wikidata_profession = None

        # print (data["results"]["bindings"])

        for binding in (data["results"]["bindings"]):
            value = binding["o"]["value"]
            if value.find("http://www.wikidata.org/") >= 0:
                # print(v)
                if value in wikidata.keys():
                    wikidata_profession = value
                    links.add(value)

        #  wenn die Wikidata die Entität als "Beruf" enthält
        if wikidata_profession:
            labels.add(load_dbpedia(item, True))
            descriptions.add(load_dbpedia(item, True, False))
            images.update(load_dbpedia_depictions(item))
            images.update(load_dbpedia_depictions(item, True))

            # add info from Wikidata
            wikid = wikidata[wikidata_profession]
            descriptions.add(wikid["description"])
            images.add(wikid["image"])

            # gibts  auch noch was in der englischen DBpedia?
            for binding in (data["results"]["bindings"]):
                value = binding["o"]["value"]

                if value.find("http://dbpedia.org/") >= 0:
                    print(value)
                    labels.add(load_dbpedia(value, True))
                    descriptions.add(load_dbpedia(value, True, False))
                    images.update(load_dbpedia_depictions(value))
                    images.update(load_dbpedia_depictions(value, True))

            print(labels-{None})
            print(descriptions-{None})
            print(images-{None})
            print(links-{None})


def lookup_details(wikidata: dict, name: str):
    "Schlägt die details eines Berufs nach und gibt es als dict zurück."

    # finde alle Resourcen die auch in EN DBpedia und Wikidata vorkommen (sameAs Links verfolgen)
    resource = "http://de.dbpedia.org/resource/" + name
    sparql = f"""
        select distinct * where {{
            <{resource}> <http://www.w3.org/2002/07/owl#sameAs> ?o .
        }} LIMIT 1000"""
    data = sparql_query(DBPEDIA_URL, sparql)

    # zum aufsammeln pro Entität
    descriptions = set()
    labels = set()
    images = set()
    links = set()

    item = "http://de.dbpedia.org/resource/" + name
    links.add(item)
    wikidata_profession = None

    for binding in (data["results"]["bindings"]):
        value = binding["o"]["value"]
        if value.find("http://www.wikidata.org/") >= 0:
            if value in wikidata.keys():
                wikidata_profession = value
                links.add(value)

    #  wenn die Wikidata die Entität als "Beruf" enthält
    if wikidata_profession:
        labels.add(load_dbpedia(item, True))
        descriptions.add(load_dbpedia(item, True, False))
        images.update(load_dbpedia_depictions(item))
        images.update(load_dbpedia_depictions(item, True))

        # add info from Wikidata
        wikid = wikidata[wikidata_profession]
        descriptions.add(wikid["description"])
        images.add(wikid["image"])

        # gibts  auch noch was in der englischen DBpedia?
        for binding in (data["results"]["bindings"]):
            value = binding["o"]["value"]

            if value.find("http://dbpedia.org/") >= 0:
                labels.add(load_dbpedia(value, True))
                descriptions.add(load_dbpedia(value, True, False))
                images.update(load_dbpedia_depictions(value))
                images.update(load_dbpedia_depictions(value, True))

        return {
            'labels': list(labels-{None}),
            'descriptions': list(descriptions-{None}),
            'images': list(images-{None}),
            'links': list(links-{None}),
        }
