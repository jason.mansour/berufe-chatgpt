import json
import logging
import os
import sys
from typing import Dict, List

import openai

import wikidata_query
from embedding_utils import embedding_from_string, get_closest

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def main():
    openai.api_key = os.environ["OPENAI_API_KEY"]

    topic = sys.argv[1]
    obj = generate_berufeseite_json(topic)
    print(json.dumps(obj, indent=2))


def generate_job_prompt(topic: str, seed=1) -> str:
    AVAILABLE_PROMPTS = [
        "Für welche Berufe ist das Thema {0} relevant?",
        "Welche Berufe sind mit dem Thema {0} verbunden?",
        "Mir macht das Thema {0} Spaß. Welche Berufe gibt es, die damit zu tun haben?",
    ]

    template = AVAILABLE_PROMPTS[seed % len(AVAILABLE_PROMPTS)]
    return template.format(topic)


def get_jobs_for_topic(topic: str) -> str:
    prompt = generate_job_prompt(topic)
    # Ask ChatGPT for a response
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "Du bist eine freundliche KI die Jugendlichen und älteren Kindern bei der Berufswahl helfen, und sie für Schulthemen motivieren soll."},
            {"role": "system", "content": "Schreibe das Ergebnis als 3-4 Zeilen Fließtext, nicht als lange Liste."},
            {"role": "user", "content": prompt}
        ]
    )
    message = response.choices[0].message.content
    return message


def generate_job_suggestions(topic: str) -> List[str]:
    prompt_template = """Wir wollen eine Liste von Berufen, für die ein bestimmtes Thema in der Schule relevant ist. Die Liste soll keine Duplikate beinhalten. Fasse ähnliche Berufe zusammen. Verwende die generische männliche Form.

Beispiel für das Thema "Elektrizität":
- Elektriker
- Ingenieur
- IT-Systemelektroniker
- Elektrotechniker
- Physiker
- Telekommunikationstechniker
- Elektronikentwickler

Was sind Berufe die zum Thema "{0}" passen?"""

    result = openai.Completion.create(
        model="text-davinci-002",
        prompt=prompt_template.format(topic),
        max_tokens=256,
        temperature=0.7,
        frequency_penalty=0.1,
        presence_penalty=0.1,
    )

    output = result.choices[0].text
    suggestions = []
    lines = output.splitlines()
    for line in lines:
        if line.startswith("- "):
            suggestions.append(line[2:])

    return suggestions


def generate_berufeseite_json(topic: str) -> dict:

    # log.info("Asking ChatGPT for some text")
    # description = get_jobs_for_topic(topic)
    # log.info(description)

    # # Generate a couple of job suggestions
    # log.info("Generating job suggestions")
    # suggestions = generate_job_suggestions(topic)
    # log.info(suggestions)

    description = "Optik ist ein interdisziplinäres Feld und bietet eine Vielzahl von Berufsmöglichkeiten. Dazu gehören optische Technologie, Augenoptik und -optometrie, Opto-Elektronik und Photonik sowie Forschung und Entwicklung in der Optikbranche. Weitere Berufe sind Physiker/in, Ingenieur/in, Techniker/in, Wissenschaftler/in und Dozent/in."
    suggestions = ['Optiker', 'Augenarzt', 'Physiker', 'Ophthalmologe',
                   'Optometrist', 'Photometrist', 'Photoniker', 'Sehhilfenoptiker']
    # suggestions = ['Physiker', 'Kieferorthopäde', 'Fotomodell', 'Videofilmer', 'Assistent für Medientechnik', 'Mediengestalter Digital und Print', 'Physiater', 'Neurologe']

    # job_name = "Veterinär"
    # job_name = "Bäckerin"

    wdobj = WikiData()
    wdobj.load()
    wdobj.generate_gpt_embeddings()

    # Wir können entweder die vorschläge von ChatGPT nehmen,
    # oder nur die die auch in WikiData sind. Wir nehmen hier letzteres.

    wikidata_suggestions = []
    for job_name in suggestions:
        # Map this to a WikiData entry
        closest = wdobj.get_closest_wikidata_entries(job_name)
        log.info(closest)
        wd_job = closest

        log.info(f"Closest WikiData entry for {job_name} -> {wd_job}")
        if wd_job not in wikidata_suggestions:
            wikidata_suggestions.append(wd_job)

    log.info("WikiData suggestions: %r", wikidata_suggestions)
    tiles = []

    for wd_job in wikidata_suggestions:
        # # wikidata = wikidata_query.query_wikidata_jobs()
        # # with open("data/wikidata_jobs_full.json", "w") as f:
        # #     json.dump(wikidata, f)

        # with open("data/wikidata_jobs_full.json", "r") as f:
        #     wikidata = json.load(f)

        # url = get_entitiy_url(wikidata, wd_job)
        # log.info("Entitiy URL: ", url)

        # Note this doesn't work for all jobs, e.g. "Kieferorthopäde"
        # although it is in WikiData
        tile = wdobj.generate_tile(wd_job)
        log.info("Kachel: %s", "Yes" if tile is not None else "No")
        tiles.append(tile)

    log.info("Output:")
    # pprint(tiles)

    full_result = {
        'topic': topic,
        'description': description,
        'tiles': tiles,
    }
    # pprint(full_result)
    return full_result


class WikiData:
    data: Dict[str, Dict]
    wiki_strings: List[str]
    wiki_embeddings: List

    def __init__(self):
        self.data = {}

    def load(self):
        with open("data/wikidata_jobs_full.json", "r", encoding='utf-8') as f:
            self.data = json.load(f)

    def query(self):
        self.data = wikidata_query.query_wikidata_jobs()

    def save(self):
        with open("data/wikidata_jobs_full.json", "w", encoding='utf-8') as f:
            json.dump(self.data, f)

    def generate_gpt_embeddings(self):
        # Filter this dict and remove all entries where entry['description'] is None
        subset = {}
        for k, v in self.data.items():
            if v['description'] is None:
                continue
            subset[k] = v
            if len(subset) > 1000:
                break

        self.wiki_strings = [v['label'] for v in subset.values()]
        self.wiki_embeddings = [embedding_from_string(
            label) for label in self.wiki_strings]

    def get_closest_wikidata_entries(self, jobname: str):
        closest = get_closest(jobname, self.wiki_strings, self.wiki_embeddings)
        return closest[0][0]

    def generate_tile(self, wd_job: str):
        wikidata = self.data

        log.info("Looking up detailed information for %s", wd_job)
        # wikidata_query.describe(wd_job, wikidata)
        details = wikidata_query.lookup_details(wikidata, wd_job)
        if details is None:
            log.info("Warning, no information found for %s", wd_job)
            return None
        # log.info("Details: ", details)
        label = details['labels'][0]
        description = details['descriptions'][0]
        images = details['images']
        # TODO: get Wikipedia link
        links = [
            {'title': "WikiData Link", 'url': l} for l in details['links']
        ]

        tile = {
            'title': label,
            'description': description,
            'images': images,
            'links': links,
        }
        return tile

    def get_entitiy_url(self, wd_job: str):
        # get the entity URL from the DB
        for k, v in self.data.items():
            if v['label'] == wd_job:
                return k
        return None


if __name__ == "__main__":
    main()
