# Simple flask server for the berufeseite api

from flask import Flask
from berufeapi import generate_berufeseite_json

app = Flask(__name__)

@app.route("/topic/<topic>")
def berufeseite(topic):
    return generate_berufeseite_json(topic)