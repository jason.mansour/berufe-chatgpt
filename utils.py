import inspect
from functools import wraps

import logging
log = logging.getLogger(__name__)

def dump_args(func):
    """
    Decorator to print function call details.

    This includes parameters names and effective values.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        func_args = inspect.signature(func).bind(*args, **kwargs).arguments
        func_args_str = ", ".join(f"{k}={v!r}" for k,v in func_args.items())
        log.info("Called %s(%s)", func.__qualname__, func_args_str)
        result = func(*args, **kwargs)
        log.info("Result: %r", result)
        return result

    return wrapper